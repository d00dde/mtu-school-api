import { assert } from "chai";
import ApiController from "../src/controller/ApiController";
import LoginChecker from "../src/cheker/LoginCheker";
import { negativeData } from "../src/fixtures/login";

describe("Test login. Negative. ", function () {
  negativeData.forEach(({ name, sendData, expected }) => {
    it(this.description + name, async () => {
      const client = new ApiController();
      const { statusCode, body } = await client.loginError(sendData);
      assert.equal(statusCode, expected.status, "Status code");
      LoginChecker.loginErrorSchema.parse(body);
      assert.include(body.uiMessage, expected.errorBody.uiMessage, "Error body");
    });
  });
});
