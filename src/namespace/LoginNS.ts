import * as zod from "zod";
import LoginChecker from "../cheker/LoginCheker";

export namespace LoginNS {
  export type SendBodyType = {
    login: string,
    password: string,
  };
  export type PositiveFixture = {
    name: string,
    sendData: SendBodyType,
    expected: {
      status: number,
      errorBody: ErrorBodyType,
    }
  };
  export type ErrorBodyType = zod.infer<typeof LoginChecker.loginErrorSchema>;
}
