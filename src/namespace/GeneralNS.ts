export namespace GeneralNS {
  export type ConfigType = {
    baseUrl: string,
    timeout: number,
  };
}
