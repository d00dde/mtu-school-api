import { config } from "./config";

declare global {
  namespace NodeJS {
    interface Global { }
  }
}

jest.setTimeout(config.timeout);
